package Controller;

import Model.*;
import UI.*;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.image.BufferedImage;
import java.io.File;
import java.sql.Time;
import java.time.LocalDateTime;
import java.util.ArrayList;


public class ProjectionController {
    Scene model = null;
    ImageIcon imageXOY;
    ImageIcon imageXOZ;
    ImageIcon imageYOZ;
    ImageIcon imagePerspective;
    ProjectionView projectionXOY;
    ProjectionView projectionXOZ;
    ProjectionView projectionYOZ;
    ProjectionView projectionPerspective;
    public static final int XOY = 1;
    public static final int XOZ = 2;
    public static final int YOZ = 3;
    Vertex activeCamera = null;
    AngleListener angleListener;


    public void setView(ProjectionView v) {
        if (v instanceof ProjectionXOY) {
            projectionXOY = v;
        } else if (v instanceof ProjectionXOZ) {
            projectionXOZ = v;
        } else if (v instanceof ProjectionYOZ) {
            projectionYOZ = v;
        } else {
            projectionPerspective = v;
        }
    }

    public void setModel(Scene scene) {
        model = scene;
    }

    public void updateView() {
        imageXOY = new ImageIcon(model.projectParallel(Scene.XOY));
        projectionXOY.updateView(imageXOY);
        imageXOZ = new ImageIcon(model.projectParallel(Scene.XOZ));
        projectionXOZ.updateView(imageXOZ);
        imageYOZ = new ImageIcon(model.projectParallel(Scene.YOZ));
        projectionYOZ.updateView(imageYOZ);

        boolean loop = false;
        if(loop){
            long lastUpdateTime = 0;
            while(true) {
                if (System.currentTimeMillis() - lastUpdateTime >= 16) {
                    lastUpdateTime = System.currentTimeMillis();
                    imagePerspective = new ImageIcon(model.projectPerspective3());
                    projectionPerspective.updateView(imagePerspective);
                }
            }
        }
        else{
            imagePerspective = new ImageIcon(model.projectPerspective3());
            projectionPerspective.updateView(imagePerspective);
        }
    }

    public void activateCamera(int x, int y, int type) {
        switch (type) {
            case XOY:
                activeCamera = model.isCamera(x, y, Scene.XOY);
                break;
            case XOZ:
                activeCamera = model.isCamera(x, y, Scene.XOZ);
                break;
            case YOZ:
                activeCamera = model.isCamera(x, y, Scene.YOZ);
                break;
        }

    }

    public void moveCamera(int x, int y, int type) {
        if (activeCamera != null) {
            switch (type) {
                case XOY:
                    model.setCameraCoords(activeCamera, x, y, Scene.XOY);
                    break;
                case XOZ:
                    model.setCameraCoords(activeCamera, x, y, Scene.XOZ);
                    break;
                case YOZ:
                    model.setCameraCoords(activeCamera, x, y, Scene.YOZ);
                    break;
            }
            updateView();
        }
    }

    public void clearActiveCamera() {
        activeCamera = null;
    }

    public void setCameraAngle(int angle) {
        model.setCameraAngle(angle);
        updateView();
    }

    // ----------------------------------------------------------------------------------------------------------------

    public void loadFromFile() {
        JFileChooser fileChooser = new JFileChooser();
        File scene, camera;

        fileChooser.setFileFilter(new FileNameExtensionFilter("SCENE FILES", "brp"));
        int result = fileChooser.showOpenDialog(null);
        if (result != JFileChooser.APPROVE_OPTION) return;
        scene = fileChooser.getSelectedFile();

        fileChooser.setFileFilter(new FileNameExtensionFilter("CAMERA FILES", "cam"));
        result = fileChooser.showOpenDialog(null);
        if (result != JFileChooser.APPROVE_OPTION) return;
        camera = fileChooser.getSelectedFile();

        model = SceneParser.read(scene);
        if (model != null) {
            Camera cam = CameraParser.read(camera);
            if (cam != null) {
                model.addCamera(cam);
                updateView();
                angleListener.setAngle(cam.angle);
            }
        }

    }

    public void saveToFile() {
        Camera camera = model.getCamera();
        if (camera != null) {
            JFileChooser chooser = new JFileChooser();
            //chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            chooser.setFileFilter(new FileNameExtensionFilter("CAMERA FILES", "cam"));
            String path = "";
            if (chooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
                path = chooser.getSelectedFile().getPath();
                CameraParser.save(camera, path);
            }
        }
    }

    public interface AngleListener {
        void setAngle(int angle);
    }



    public void setAngleListener(AngleListener listener) {
        angleListener = listener;
    }
}
