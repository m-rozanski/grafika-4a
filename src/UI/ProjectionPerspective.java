package UI;

import Controller.ProjectionController;

import javax.swing.*;

public class ProjectionPerspective extends JLabel implements ProjectionView {
    private ProjectionController controller;

    public void setControllerPerspective(ProjectionController controller) {
        this.controller = controller;
    }

    public void updateView(Icon icon) {
        setIcon(icon);
        //System.out.println("updated");
    }
}
