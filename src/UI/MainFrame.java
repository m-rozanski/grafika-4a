package UI;

import Controller.ProjectionController;
import Model.Camera;
import Model.CameraParser;
import Model.Scene;
import Model.SceneParser;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.Arrays;

public class MainFrame extends JFrame implements ProjectionController.AngleListener {
    private JPanel projections;
    private ProjectionXOY xoy;
    private ProjectionXOZ xoz;
    private ProjectionYOZ yoz;
    private ProjectionPerspective perspective;
    private JPanel tools;
    private Scene scene;
    private Camera camera;
    private ProjectionController projectionController;
    private JButton buttonSave;
    private JButton buttonLoad;
    private JSlider slider;
    private String sliderLabelText = "Set camera angle (1-89)";
    private JLabel sliderLabel;

    public MainFrame() {
        super();
        initUI();
        addListeners();
        setVisible(true);
        projectionController.updateView();
    }

    private void initUI() {
        this.setTitle("Grafika 4a");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(new BorderLayout());

        projections = new JPanel();
        projections.setLayout(new GridLayout(2, 2, 5, 5));

        xoy = new ProjectionXOY();
        //xoy.setMinimumSize(new Dimension(500, 300));
        //xoy.setMaximumSize(new Dimension(500, 300));
        //xoy.setPreferredSize(new Dimension(500, 300));

        xoz = new ProjectionXOZ();
        //xoz.setMinimumSize(new Dimension(500, 300));
        //xoz.setMaximumSize(new Dimension(500, 300));
        //xoz.setPreferredSize(new Dimension(500, 300));

        yoz = new ProjectionYOZ();
        //yoz.setMinimumSize(new Dimension(500, 300));
        //yoz.setMaximumSize(new Dimension(500, 300));
        //yoz.setPreferredSize(new Dimension(500, 300));

        perspective = new ProjectionPerspective();

        JPanel xoy_container = new JPanel(new GridBagLayout());
        JPanel xoz_container = new JPanel(new GridBagLayout());
        JPanel yoz_container = new JPanel(new GridBagLayout());
        JPanel perspective_container = new JPanel(new GridBagLayout());
        xoy_container.add(xoy);
        xoz_container.add(xoz);
        yoz_container.add(yoz);
        perspective_container.add(perspective);
        projections.add(new JScrollPane(xoy_container, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS));
        projections.add(new JScrollPane(xoz_container, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS));
        projections.add(new JScrollPane(yoz_container, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS));
        projections.add(new JScrollPane(perspective_container, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS));

        //projections.setMinimumSize(new Dimension(1000, 500));
        //projections.setMaximumSize(new Dimension(1000, 500));
        //projections.setPreferredSize(new Dimension(1000, 500));

        scene = SceneParser.read(new File(".\\SceneParserTester.brp"));
        camera = CameraParser.read(new File(".\\CameraParserTester.cam"));
        scene.addCamera(camera);
        //scene = new Scene();

        projectionController = new ProjectionController();
        projectionController.setAngleListener(this);
        projectionController.setModel(scene);
        projectionController.setView(xoy);
        xoy.setControllerXOY(projectionController);


        //projectionController.setModel(scene);
        projectionController.setView(xoz);
        xoz.setControllerXOZ(projectionController);


        projectionController.setView(yoz);
        yoz.setControllerYOZ(projectionController);

        projectionController.setView(perspective);
        perspective.setControllerPerspective(projectionController);

        projectionController.updateView();

        // init tools
        tools = new JPanel();
        tools.setLayout(new BorderLayout());
        JPanel cameraContainer = new JPanel();
        cameraContainer.setLayout(new BoxLayout(cameraContainer, BoxLayout.PAGE_AXIS));
        JPanel buttonContainer = new JPanel();
        buttonContainer.setLayout(new BoxLayout(buttonContainer, BoxLayout.PAGE_AXIS));
        sliderLabel = new JLabel(sliderLabelText);
        cameraContainer.add(sliderLabel);
        slider = new JSlider(1, 89);
        cameraContainer.add(slider);
        buttonSave = new JButton("SAVE");
        buttonLoad = new JButton("LOAD");
        buttonContainer.add(buttonLoad);
        buttonContainer.add(buttonSave);
        tools.add(cameraContainer, BorderLayout.WEST);
        tools.add(buttonContainer, BorderLayout.EAST);

        getContentPane().add(projections, BorderLayout.CENTER);
        getContentPane().add(tools, BorderLayout.SOUTH);
        setSize(new Dimension(1280, 720));
        setResizable(true);
        setLocationRelativeTo(null);
        pack();
        System.out.println(Arrays.toString(scene.faces));
    }

    private void addListeners() {
        buttonLoad.addActionListener(actionEvent -> projectionController.loadFromFile());
        buttonSave.addActionListener(actionEvent -> projectionController.saveToFile());
        slider.addChangeListener(changeEvent -> {
                    sliderLabel.setText(sliderLabelText + ": " + slider.getValue());
                    projectionController.setCameraAngle(slider.getValue());
                }
        );
    }

    @Override
    public void setAngle(int angle) {
        slider.setValue(angle);
    }
}
