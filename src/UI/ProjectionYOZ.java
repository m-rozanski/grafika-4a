package UI;

import Controller.ProjectionController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;


public class ProjectionYOZ extends JLabel implements ProjectionView, MouseListener, MouseMotionListener {
    private ProjectionController controller;

    public ProjectionYOZ() {
        super();
        addMouseListener(this);
        addMouseMotionListener(this);
    }

    public void setControllerYOZ(ProjectionController controller) {
        this.controller = controller;
    }

    public void updateView(Icon icon) {
        setIcon(icon);
    }

    // ----------------------------------------------------------------------------------------------------------------

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {

    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {
        controller.activateCamera(mouseEvent.getX(), mouseEvent.getY(), ProjectionController.YOZ);
    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {
        controller.clearActiveCamera();
    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseDragged(MouseEvent mouseEvent) {
        controller.moveCamera(mouseEvent.getX(), mouseEvent.getY(), ProjectionController.YOZ);
    }

    @Override
    public void mouseMoved(MouseEvent mouseEvent) {
    }
}
