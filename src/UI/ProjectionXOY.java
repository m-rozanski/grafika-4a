package UI;

import Controller.ProjectionController;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;


public class ProjectionXOY extends JLabel implements ProjectionView, MouseListener, MouseMotionListener {
    private ProjectionController controller;

    public ProjectionXOY() {
        super();
        addMouseListener(this);
        addMouseMotionListener(this);
    }

    public void setControllerXOY(ProjectionController controller) {
        this.controller = controller;
    }

    public void updateView(Icon icon) {
        setIcon(icon);
    }

    // ----------------------------------------------------------------------------------------------------------------

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {

    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {
        controller.activateCamera(mouseEvent.getX(), mouseEvent.getY(), ProjectionController.XOY);
    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {
        controller.clearActiveCamera();
    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseDragged(MouseEvent mouseEvent) {
        controller.moveCamera(mouseEvent.getX(), mouseEvent.getY(), ProjectionController.XOY);
    }

    @Override
    public void mouseMoved(MouseEvent mouseEvent) {
    }
}
