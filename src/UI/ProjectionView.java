package UI;

import javax.swing.*;

public interface ProjectionView {
    public void updateView(Icon icon);
}
