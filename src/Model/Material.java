package Model;

public class Material {
    int R;
    int G;
    int B;
    int kd;
    int ks;
    int g;

    public Material(int R, int G, int B, int kd, int ks, int g) {
        setParams(R, G, B, kd, ks, g);
    }

    public Material(int[] params) {
        if (params.length == 6)
            setParams(params[0], params[1], params[2], params[3], params[4], params[5]);
        else
            setParams(0, 0, 0, 0, 0, 0);
    }

    private void setParams(int R, int G, int B, int kd, int ks, int g) {
        this.R = R;
        this.G = G;
        this.B = B;
        this.kd = kd;
        this.ks = ks;
        this.g = g;
    }

    @Override
    public String toString(){
        return "Material: " + R + " " + G + " " + B + " " + kd + " " + ks + " " + g;
    }
}
