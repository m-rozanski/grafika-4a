package Model;

import java.util.Arrays;

public class Camera {
    public Vertex start;
    public Vertex end;
    public int angle;
    public int near = 10;
    public int far = 1000;

    public Camera(int[] Start, int[] End, int Angle) {
        start = new Vertex(Start);
        end = new Vertex(End);
        angle = Angle;
    }

    public double[] getVector(){
        return new double[]{
                end.x - start.x,
                end.y - start.y,
                end.z - start.z,
                1
        };
    }

    @Override
    public String toString() {
        return "Camera:\nStart: " + start + "\nEnd: " + end + "\nAngle: " + angle;
    }
}
