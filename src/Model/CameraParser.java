package Model;

import java.io.*;

public class CameraParser {
    public static Camera read(File file) {
        int[] start = new int[3];   // środek kamery
        int[] end = new int[3]; // środek obrazu
        int angle = 1;  // rozwarcie kamery

        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line;
            String[] values;
            Integer value;

            line = reader.readLine();
            if (line == null) return null;
            values = line.split(" ");
            if (values.length != 3) return null;
            for (int i = 0; i < 3; i++) {
                value = tryParseInt(values[i]);
                if (value == null) return null;
                start[i] = value;
            }

            line = reader.readLine();
            if (line == null) return null;
            values = line.split(" ");
            if (values.length != 3) return null;
            for (int i = 0; i < 3; i++) {
                value = tryParseInt(values[i]);
                if (value == null) return null;
                end[i] = value;
            }

            line = reader.readLine();
            if (line == null) return null;
            value = tryParseInt(line);
            if (value == null) return null;
            angle = value;

            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        return new Camera(start, end, angle);
    }

    public static void save(Camera camera, String path) {
        if (!path.endsWith(".cam"))
            path += ".cam";

        try {
            PrintWriter out = new PrintWriter(path);

            out.print(camera.start.x + " ");
            out.print(camera.start.y + " ");
            out.println(camera.start.z);
            out.print(camera.end.x + " ");
            out.print(camera.end.y + " ");
            out.println(camera.end.z);
            out.print(camera.angle);

            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    // ----------------------------------------------------------------------------------------------------------------
    private static Integer tryParseInt(String val) {
        try {
            return Integer.parseInt(val);
        } catch (NumberFormatException e) {
            return null;
        }
    }
}
