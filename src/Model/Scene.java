package Model;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;


public class Scene {
    private static final int INITIAL_CANVAS_X_SIZE = 850;
    private static final int INITIAL_CANVAS_Y_SIZE = 500;

    private Vertex[] vertices;
    public Face[] faces;
    private Light light;
    private Camera camera = null;

    // [0] - min, [1] - max
    private int[] xx;
    private int[] yy;
    private int[] zz;

    private int canvas_size_x;
    private int canvas_size_y;
    private int center_x;
    private int center_y;
    Matrix viewMatrix;
    Vertex[] fovPoints;
    Matrix[] transforms;

    public Scene() {
        canvas_size_x = INITIAL_CANVAS_X_SIZE;
        canvas_size_y = INITIAL_CANVAS_Y_SIZE;
        updateCenterCoordinates();
    }

    public Scene(Vertex[] vertices, Face[] faces, Light light) {
        this.vertices = vertices;
        this.faces = faces;
        this.light = light;
        initializeMinMaxValues();
        findMinMaxValues();
        canvas_size_x = INITIAL_CANVAS_X_SIZE;
        canvas_size_y = INITIAL_CANVAS_Y_SIZE;
        updateCenterCoordinates();
    }

    public void addCamera(Camera camera) {
        this.camera = camera;
        viewMatrix = getViewMatrix();
        setFovPoints();
    }

    public Camera getCamera() {
        return camera;
    }

    public void loadScene(Vertex[] vertices, Face[] faces, Light light) {
        this.vertices = vertices;
        this.faces = faces;
        this.light = light;
        initializeMinMaxValues();
        findMinMaxValues();
    }

    private void initializeMinMaxValues() {
        xx = new int[2];
        yy = new int[2];
        zz = new int[2];
    }

    private void findMinMaxValues() {
        for (Vertex v : vertices) {
            if (xx[0] > v.x) xx[0] = v.x;
            else if (xx[1] < v.x) xx[1] = v.x;

            if (yy[0] > v.y) yy[0] = v.y;
            else if (yy[1] < v.y) yy[1] = v.y;

            if (zz[0] > v.z) zz[0] = v.z;
            else if (zz[1] < v.z) zz[1] = v.z;
        }
    }

    private void updateCenterCoordinates() {
        center_x = canvas_size_x / 2;
        center_y = canvas_size_y / 2;
    }

    // ----------------------------------------------------------------------------------------------------------------
    String path = "C:\\Users\\Mati\\Desktop\\data\\";
    public static final int XOY = 1;
    public static final int XOZ = 2;
    public static final int YOZ = 3;

    public void generateProjections() {
        projectParallel(XOY);
        projectParallel(XOZ);
        projectParallel(YOZ);
    }

    public BufferedImage projectParallel(int type) {
        BufferedImage image = new BufferedImage(canvas_size_x, canvas_size_y, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = image.createGraphics();
        g2d.setColor(Color.WHITE);
        if (faces != null) {
            Matrix M;
            int x, y;
            switch (type) {
                case XOY:
                    M = Matrix.getParallelProjectionMatrix(Matrix.XOY);
                    x = 0;
                    y = 1;
                    break;
                case XOZ:
                    M = Matrix.getParallelProjectionMatrix(Matrix.XOZ);
                    x = 0;
                    y = 2;
                    break;
                case YOZ:
                default:
                    M = Matrix.getParallelProjectionMatrix(Matrix.YOZ);
                    x = 2;
                    y = 1;
                    break;
            }

            int[] projectedVertex;
            int[][] points = new int[3][];  // 3 points because of triangle representation

            for (int i = 0; i < faces.length; i++) {
                for (int j = 0; j < 3; j++) {
                    projectedVertex = M.transform(faces[i].vertices[j]);
                    points[j] = new int[]{center_x + projectedVertex[x], center_y - projectedVertex[y]};
                }

                g2d.drawLine(points[0][0], points[0][1], points[1][0], points[1][1]);
                g2d.drawLine(points[1][0], points[1][1], points[2][0], points[2][1]);
                g2d.drawLine(points[0][0], points[0][1], points[2][0], points[2][1]);
            }

            int[] projectedCamera = M.transform(camera.end);
            g2d.setColor(Color.RED);
            g2d.drawRect(center_x + projectedCamera[x] - 5, center_y - projectedCamera[y] - 5, 10, 10);

            projectedCamera = M.transform(camera.start);
            g2d.setColor(Color.BLUE);
            g2d.drawRect(center_x + projectedCamera[x] - 5, center_y - projectedCamera[y] - 5, 10, 10);

            M = transforms[0].multiply(transforms[1]);
            M = M.multiply(transforms[2]);
            g2d.setColor(Color.YELLOW);

            int[][] cameraV = new int[][]{
                    fovPoints[0].getXYZW(),
                    fovPoints[1].getXYZW(),
                    fovPoints[2].getXYZW(),
                    fovPoints[3].getXYZW()
            };
            int[][] pointsCameraV = new int[][]{
                    {center_x + cameraV[0][x], center_y - cameraV[0][y]},
                    {center_x + cameraV[1][x], center_y - cameraV[1][y]},
                    {center_x + cameraV[2][x], center_y - cameraV[2][y]},
                    {center_x + cameraV[3][x], center_y - cameraV[3][y]}
            };
            //System.out.println(pointsCameraV.length);
            //Polygon polygon = new Polygon();
            g2d.drawLine(pointsCameraV[0][0], pointsCameraV[0][1], pointsCameraV[1][0], pointsCameraV[1][1]);
            g2d.drawLine(pointsCameraV[1][0], pointsCameraV[1][1], pointsCameraV[3][0], pointsCameraV[3][1]);
            g2d.drawLine(pointsCameraV[2][0], pointsCameraV[2][1], pointsCameraV[3][0], pointsCameraV[3][1]);
            g2d.drawLine(pointsCameraV[2][0], pointsCameraV[2][1], pointsCameraV[0][0], pointsCameraV[0][1]);
            for (int[] p : pointsCameraV) {
                //polygon.addPoint(p[0], p[1]);
                g2d.drawLine(p[0], p[1], center_x + projectedCamera[x], center_y - projectedCamera[y]);
            }
            //g2d.drawPolygon(polygon);

            g2d.dispose();
        }

        return image;
    }

    private boolean acceptFace(Face f, int d, Matrix M) {
        int[] ver;
        for (Vertex v : f.vertices) {
            ver = M.transform(v);
            if (ver[2] < d) {
                return false;
            }
        }

        return true;
    }

    double theta = 0.05;

    public BufferedImage projectPerspective3() {
        BufferedImage image = new BufferedImage(canvas_size_x, canvas_size_y, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = image.createGraphics();
        g2d.setColor(Color.WHITE);
        Matrix rotateX, rotateZ;
        theta += 0.02;
        rotateX = new Matrix(theta, Matrix.ROTATE_X);
        rotateZ = new Matrix(theta / 2, Matrix.ROTATE_Z);
        Matrix M = viewMatrix.multiply(Matrix.getPerspectiveMatrix2(canvas_size_y, canvas_size_x, camera.near, camera.far, camera.angle));
        Matrix rotate = rotateZ.multiply(rotateX);

        // accumulate triangles to draw
        ArrayList<Face> facesToShow = new ArrayList<>();

        // draw triangles
        for (Face face : faces) {
            //double[] v1 = rotate.transformAndNormalizeD(face.vertices[0]);
            //double[] v2 = rotate.transformAndNormalizeD(face.vertices[1]);
            //double[] v3 = rotate.transformAndNormalizeD(face.vertices[2]);
            double[] v1 = new double[]{
                    face.vertices[0].x,
                    face.vertices[0].y,
                    face.vertices[0].z,
                    face.vertices[0].w,
            };
            double[] v2 = new double[]{
                    face.vertices[1].x,
                    face.vertices[1].y,
                    face.vertices[1].z,
                    face.vertices[1].w,
            };
            double[] v3 = new double[]{
                    face.vertices[2].x,
                    face.vertices[2].y,
                    face.vertices[2].z,
                    face.vertices[2].w,
            };

            // offset
            //v1[2] += 250;
            //v2[2] += 250;
            //v3[2] += 250;

            // normals
            double[] normal, line1, line2;
            line1 = subVector(v2, v1);
            line2 = subVector(v3, v1);
            normal = crossProduct(line1, line2);
            normal = normalizeVector(normal);
            //double[] vCamera = new double[] {0,0,0,0};

            double[] vCamera = new double[]{
                    camera.start.x,
                    camera.start.y,
                    camera.start.z,
                    camera.start.w,
            };
            double[] vCameraRay = subVector(v1, vCamera);


            //projection
            if (dotProduct(normal, vCameraRay) < 0) {
                v1[3] = 1;
                v2[3] = 1;
                v3[3] = 1;

                double[] proj_v1 = M.transformD(v1);
                double[] proj_v2 = M.transformD(v2);
                double[] proj_v3 = M.transformD(v3);

                double z1 = proj_v1[2];
                double z2 = proj_v2[2];
                double z3 = proj_v3[2];
                proj_v1 = divVector(proj_v1, proj_v1[3]);
                proj_v2 = divVector(proj_v2, proj_v2[3]);
                proj_v3 = divVector(proj_v3, proj_v3[3]);

                // scale
                proj_v1[0] += 1.0;
                proj_v1[1] += 1.0;
                proj_v2[0] += 1.0;
                proj_v2[1] += 1.0;
                proj_v3[0] += 1.0;
                proj_v3[1] += 1.0;

                proj_v1[0] *= (0.5 * canvas_size_x);
                proj_v1[1] *= (0.5 * canvas_size_y);
                proj_v2[0] *= (0.5 * canvas_size_x);
                proj_v2[1] *= (0.5 * canvas_size_y);
                proj_v3[0] *= (0.5 * canvas_size_x);
                proj_v3[1] *= (0.5 * canvas_size_y);

                if (z1 > camera.near && z2 > camera.near && z3 > camera.near) {
                    Vertex[] vertices = new Vertex[]{
                            new Vertex((int) Math.round(proj_v1[0]), (int) Math.round(proj_v1[1]), (int) Math.round(z1)),
                            new Vertex((int) Math.round(proj_v2[0]), (int) Math.round(proj_v2[1]), (int) Math.round(z2)),
                            new Vertex((int) Math.round(proj_v3[0]), (int) Math.round(proj_v3[1]), (int) Math.round(z3))
                    };

                    facesToShow.add(new Face(vertices, face.material));
                }
            }
        }

        // sort faces to draw
        Collections.sort(facesToShow, (f1, f2) -> {
            double z1 = (f1.vertices[0].z + f1.vertices[1].z + f1.vertices[2].z) / 3.0;
            double z2 = (f2.vertices[0].z + f2.vertices[1].z + f2.vertices[2].z) / 3.0;

            if (z1 < z2) return 1;

            else return -1;
        });


        Polygon polygon = new Polygon();
        for (Face face : facesToShow) {
            polygon.reset();
            polygon.addPoint(face.vertices[0].x, canvas_size_y - face.vertices[0].y);
            polygon.addPoint(face.vertices[1].x, canvas_size_y - face.vertices[1].y);
            polygon.addPoint(face.vertices[2].x, canvas_size_y - face.vertices[2].y);

            Color color = new Color(face.material.R, face.material.G, face.material.B);
            g2d.setColor(color);
            g2d.fill(polygon);
            g2d.setColor(Color.WHITE);
            g2d.draw(polygon);
        }

        facesToShow.clear();
        return image;
    }

    public BufferedImage projectPerspective() {
        BufferedImage image = new BufferedImage(canvas_size_x, canvas_size_y, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = image.createGraphics();
        g2d.setColor(Color.WHITE);

        // matrix
        int d = viewMatrix.transform(camera.start)[2];  // distance from observer to 0,0,0
        //System.out.println(d);
        //Matrix M = viewMatrix.multiply(Matrix.getPerspectiveMatrix(d));
        Matrix M = viewMatrix.multiply(Matrix.getPerspectiveMatrix2(canvas_size_y, canvas_size_x, d + camera.near, d + camera.far, camera.angle));

        int[] projectedVertex;
        int[][] points = new int[3][];  // 3 points because of triangle representation
        ArrayList<Face> otherFaces = new ArrayList<>();
        for (Face face : faces) {
            //if (acceptFace(face, d, M)) {
            for (int j = 0; j < 3; j++) {
                projectedVertex = M.transform(face.vertices[j]);
                if (face.vertices[j].x == 0 && face.vertices[j].y == 0 && face.vertices[j].z == 0) {
                    //System.out.println("Przed transformacją: " + face.vertices[j].toString());
                    //System.out.println("Transformacja do układu obserwatora: " + Arrays.toString(viewMatrix.transform(face.vertices[j])));
                    //System.out.println("Projekcja: " + Arrays.toString(projectedVertex));
                }
                double w = projectedVertex[3];
                int[] other = new int[]{(int) (((projectedVertex[0] / w) + 1) * (0.5 * canvas_size_x)), (int) (((projectedVertex[1] / w) + 1) * (0.5 * canvas_size_y))};
                points[j] = new int[]{other[0], canvas_size_y - other[1]};
            }

            g2d.drawLine(points[0][0], points[0][1], points[1][0], points[1][1]);
            g2d.drawLine(points[1][0], points[1][1], points[2][0], points[2][1]);
            g2d.drawLine(points[0][0], points[0][1], points[2][0], points[2][1]);

        }



        /*
        g2d.setColor(Color.RED);
        Vertex z_inf_minus = new Vertex(0,0,-1000);
        Vertex z_inf_plus = new Vertex(0,0,1000);
        int[] z_1 = M.transform(z_inf_minus);
        int[] z_2 = M.transform(z_inf_plus);
        int[] other = new int[]{(int) (((z_1[0] / z_1[3]) + 1) * (0.5 * canvas_size_x)), (int) (((z_1[1] / z_1[3]) + 1) * (0.5 * canvas_size_y))};
        int[] other2 = new int[]{(int) (((z_2[0] / z_2[3]) + 1) * (0.5 * canvas_size_x)), (int) (((z_2[1] / z_2[3]) + 1) * (0.5 * canvas_size_y))};
        int [] p1 = new int[]{other[0], canvas_size_y - other[1]};
        int [] p2 = new int[]{other2[0], canvas_size_y - other2[1]};
        g2d.drawLine(p1[0], p1[1], p2[0], p2[1]);
        */
        //ImageUtils.saveImage(image, path + "perspective");
        return image;
    }

    private double[] addVector(double[] v1, double[] v2) {
        return new double[]{
                v1[0] + v2[0],
                v1[1] + v2[1],
                v1[2] + v2[2]
        };
    }

    private double[] subVector(double[] v1, double[] v2) {
        return new double[]{
                v1[0] - v2[0],
                v1[1] - v2[1],
                v1[2] - v2[2]
        };
    }

    private double[] mulVector(double[] v1, double k) {
        return new double[]{
                v1[0] * k,
                v1[1] * k,
                v1[2] * k
        };
    }

    private double[] divVector(double[] v1, double k) {
        return new double[]{
                v1[0] / k,
                v1[1] / k,
                v1[2] / k
        };
    }

    private double dotProduct(double[] v1, double[] v2) {
        return v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2];
    }

    private double vectorLength(double[] v) {
        return Math.sqrt(dotProduct(v, v));
    }

    private double[] normalizeVector(double[] v) {
        double l = vectorLength(v);
        return new double[]{
                v[0] / l,
                v[1] / l,
                v[2] / l,
                1
        };
    }

    private double[] crossProduct(double[] v1, double[] v2) {
        return new double[]{
                v1[1] * v2[2] - v1[2] * v2[1],
                v1[2] * v2[0] - v1[0] * v2[2],
                v1[0] * v2[1] - v1[1] * v2[0],
        };
    }

    private boolean testVisibility(Face face) {
        double[] normal, line1, line2;

        line1 = new double[]{
                face.vertices[1].x - face.vertices[0].x,
                face.vertices[1].y - face.vertices[0].y,
                face.vertices[1].z - face.vertices[0].z
        };
        line2 = new double[]{
                face.vertices[2].x - face.vertices[0].x,
                face.vertices[2].y - face.vertices[0].y,
                face.vertices[2].z - face.vertices[0].z
        };
        normal = new double[]{
                line1[1] * line2[2] - line1[2] * line2[1],
                line1[2] * line2[0] - line1[0] * line2[2],
                line1[0] * line2[1] - line1[1] * line2[0],
        };
        double l = Math.sqrt(normal[0] * normal[0] + normal[1] * normal[1] + normal[2] * normal[2]);
        // normalize normal
        normal[0] /= l;
        normal[1] /= l;
        normal[2] /= l;

        double[] vCamera = new double[]{
                camera.end.x - camera.start.x,
                camera.end.y - camera.start.y,
                camera.end.z - camera.start.z
        };

        if (normal[0] * (face.vertices[0].x - vCamera[0]) +
                normal[1] * (face.vertices[0].y - vCamera[1]) +
                normal[2] * (face.vertices[0].z - vCamera[2]) < 0) {
            return true;
        }
        return false;
    }

    // transformacja do układu obserwatora, obserwator leży na ujemnych warttościach osi z, ppunkt patrzenia to środek układu 0,0,0
    private Matrix getViewMatrix() {
        Matrix viewMatrix;
        if (false) {
            Matrix translationMatrix = new Matrix(-camera.end.x, -camera.end.y, -camera.end.z, Matrix.TRANSLATION);
            int[] newCameraStart = translationMatrix.transform(camera.start);
            Vertex newCameraStartVertex = new Vertex(newCameraStart);
            viewMatrix = translationMatrix; // przypisanie translacji
            transforms = new Matrix[3];
            transforms[0] = new Matrix(camera.end.x, camera.end.y, camera.end.z, Matrix.TRANSLATION);

            // ROTATE Y
            double alpha = Math.PI - Math.atan2(newCameraStartVertex.x, newCameraStartVertex.z);
            Matrix rotateYMatrix = new Matrix(alpha, Matrix.ROTATE_Y);
            transforms[1] = new Matrix(-alpha, Matrix.ROTATE_Y);
            newCameraStart = rotateYMatrix.transform(newCameraStartVertex);
            newCameraStartVertex.setXYZW(newCameraStart);
            viewMatrix = viewMatrix.multiply(rotateYMatrix);    // przypisanie pierwszej rotacji do macierzy

            // ROTATE X
            alpha = -Math.PI / 2 - Math.atan2(newCameraStartVertex.z, newCameraStartVertex.y);
            Matrix rotateXMatrix = new Matrix(alpha, Matrix.ROTATE_X);
            transforms[2] = new Matrix(-alpha, Matrix.ROTATE_X);
            viewMatrix = viewMatrix.multiply(rotateXMatrix);   // przypisanie drugiej rotacji
        } else {
            double cameraLength = vectorLength(camera.getVector());
            double lastTranslation = cameraLength - camera.near;

            Matrix translationMatrix = new Matrix(-camera.end.x, -camera.end.y, -camera.end.z, Matrix.TRANSLATION);
            double[] newCameraStart = translationMatrix.transformD(camera.start);
            //Vertex newCameraStartVertex = new Vertex(newCameraStart);
            viewMatrix = translationMatrix; // przypisanie translacji
            transforms = new Matrix[3];
            transforms[0] = new Matrix(camera.end.x, camera.end.y, camera.end.z, Matrix.TRANSLATION);   // odwrotna transformacja

            // ROTATE Y
            double alpha = Math.PI - Math.atan2(newCameraStart[0], newCameraStart[2]);
            Matrix rotateYMatrix = new Matrix(alpha, Matrix.ROTATE_Y);
            transforms[1] = new Matrix(-alpha, Matrix.ROTATE_Y);    //odwrócony obrót
            newCameraStart = rotateYMatrix.transformD(newCameraStart);
            //newCameraStartVertex.setXYZW(newCameraStart);
            viewMatrix = viewMatrix.multiply(rotateYMatrix);    // przypisanie pierwszej rotacji do macierzy

            // ROTATE X
            alpha = -Math.PI / 2 - Math.atan2(newCameraStart[2], newCameraStart[1]);
            Matrix rotateXMatrix = new Matrix(alpha, Matrix.ROTATE_X);
            transforms[2] = new Matrix(-alpha, Matrix.ROTATE_X);    //odwrócony drugi obrót
            viewMatrix = viewMatrix.multiply(rotateXMatrix);   // przypisanie drugiej rotacji
            //transforms[3] = new Matrix(0, 0, -lastTranslation, Matrix.TRANSLATION);
            viewMatrix = viewMatrix.multiply(new Matrix(0, 0, lastTranslation, Matrix.TRANSLATION));
        }

        return viewMatrix;
    }

    private void setFovPoints() {
        Matrix inv = transforms[2].multiply(transforms[1]);
        inv = inv.multiply(transforms[0]);
        if (inv != null) {
            //double d = viewMatrix.transform(camera.start)[2];   //z
            double d = vectorLength(camera.getVector());
            int w2 = (int) Math.round(d * Math.tan(Math.toRadians(camera.angle / 2.0)));
            int h2 = (int) Math.round(w2 * ((double) canvas_size_y / (double) canvas_size_x));
            Vertex upRight = new Vertex(w2, h2, 0);
            Vertex downRight = new Vertex(w2, -h2, 0);
            Vertex downLeft = new Vertex(-w2, -h2, 0);
            Vertex upLeft = new Vertex(-w2, h2, 0);

            upRight = new Vertex(inv.transform(upRight));
            downRight = new Vertex(inv.transform(downRight));
            downLeft = new Vertex(inv.transform(downLeft));
            upLeft = new Vertex(inv.transform(upLeft));

            upRight.normalize();
            downRight.normalize();
            upLeft.normalize();
            downLeft.normalize();

            fovPoints = new Vertex[]{upRight, upLeft, downRight, downLeft};
        }
    }

    /*
    private int[] cartesianCoordinatesToArrayIndices(int[] coordinates) {
        int[] indices = new int[1];
        if (coordinates[0] < 0 && coordinates[1]) {

        }

        return indices;
    }

    private int[] arrayIndiciesToCartesianCoordinates() {
        int[] coordinates = new int[1];
        return coordinates;
    }
    */

    public Vertex isCamera(int xx, int yy, int type) {
        Matrix M;
        int x, y;
        if (camera != null) {
            switch (type) {
                case XOY:
                    M = Matrix.getParallelProjectionMatrix(Matrix.XOY);
                    x = 0;
                    y = 1;
                    break;
                case XOZ:
                    M = Matrix.getParallelProjectionMatrix(Matrix.XOZ);
                    x = 0;
                    y = 2;
                    break;
                case YOZ:
                default:
                    M = Matrix.getParallelProjectionMatrix(Matrix.YOZ);
                    x = 2;
                    y = 1;
                    break;
            }

            int[] projectedVertex = M.transform(camera.start);
            projectedVertex[x] = center_x + projectedVertex[x];
            projectedVertex[y] = center_y - projectedVertex[y];

            if (projectedVertex[x] - 5 <= xx && projectedVertex[y] - 5 <= yy)
                if (projectedVertex[x] + 5 >= xx && projectedVertex[y] + 5 >= yy)
                    return camera.start;

            projectedVertex = M.transform(camera.end);
            projectedVertex[x] = center_x + projectedVertex[x];
            projectedVertex[y] = center_y - projectedVertex[y];

            if (projectedVertex[x] - 5 <= xx && projectedVertex[y] - 5 <= yy)
                if (projectedVertex[x] + 5 >= xx && projectedVertex[y] + 5 >= yy)
                    return camera.end;
        }

        return null;
    }

    public void setCameraCoords(Vertex vertex, int xx, int yy, int type) {
        switch (type) {
            case XOY:
                vertex.x = -center_x + xx;
                vertex.y = -yy + center_y;
                break;
            case XOZ:
                vertex.x = -center_x + xx;
                vertex.z = -yy + center_y;
                break;
            case YOZ:
                vertex.z = -center_x + xx;
                vertex.y = -yy + center_y;
            default:
                break;
        }

        viewMatrix = getViewMatrix();
        setFovPoints();
    }

    public void setCameraAngle(int angle) {
        if (camera != null) camera.angle = angle;
        setFovPoints();
    }


    // ----------------------------------------------------------------------------------------------------------------


    @Override
    public String toString() {
        return "Scene: " + Arrays.deepToString(vertices) + "\n" + Arrays.deepToString(faces) + " \n" + light + camera;
    }
}
