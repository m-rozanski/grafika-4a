package Model;


public class Vertex {
    public int x;
    public int y;
    public int z;
    public int w = 1;


    public Vertex(int X, int Y, int Z) {
        setXYZ(X, Y, Z);
    }

    public Vertex(int X, int Y, int Z, int W) {
        setXYZW(X, Y, Z, W);
    }

    public Vertex(int[] args){
        if(args.length == 3)
            setXYZ(args);
        else if(args.length == 4)
            setXYZW(args);
        else
            setXYZW(0,0,0, 1);
    }

    public void setXYZ(int[] xyz) {
        x = xyz[0];
        y = xyz[1];
        z = xyz[2];
    }

    public void setXYZW(int[] xyzw) {
        x = xyzw[0];
        y = xyzw[1];
        z = xyzw[2];
        w = xyzw[3];
    }

    public void setXYZ(int X, int Y, int Z) {
        x = X;
        y = Y;
        z = Z;
    }

    public void setXYZW(int X, int Y, int Z, int W) {
        x = X;
        y = Y;
        z = Z;
        w = W;
    }

    public void setX(int x){
        this.x = x;
    }

    public void setY(int y){
        this.y = y;
    }

    public void setZ(int z){
        this.z = z;
    }

    public int[] getXYZ() {
        return new int[]{x, y, z};
    }

    public int[] getXYZW() {
        return new int[]{x, y, z, w};
    }

    public void normalize() {
        if (w != 1 && w != 0) {
            x = (int) Math.round((double) x / w);
            y = (int) Math.round((double) y / w);
            z = (int) Math.round((double) z / w);
            w = 1;
        }
    }

    @Override
    public String toString() {
        return "(" + x + "," + y + "," + z + "," + w + ")";
    }
}