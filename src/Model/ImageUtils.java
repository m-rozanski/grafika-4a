package Model;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ImageUtils {
    public static boolean saveImage(BufferedImage image, String path) {
        if (!path.endsWith(".jpg")) {
            path += ".jpg";
        }
        File file = new File(path);

        try {
            ImageIO.write(image, "jpg", file);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }
}
