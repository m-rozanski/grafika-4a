package Model;


public class Matrix {
    private double[][] matrix;
    private static final int SIZE = 4;

    public static final int SCALE = 1;
    public static final int TRANSLATION = 2;
    public static final int ROTATE_X = 3;
    public static final int ROTATE_Y = 4;
    public static final int ROTATE_Z = 5;

    private static Matrix parallel_XOY = null;
    private static Matrix parallel_XOZ = null;
    private static Matrix parallel_YOZ = null;

    public static final int XOY = 6;
    public static final int XOZ = 7;
    public static final int YOZ = 8;


    public Matrix() {
        matrix = new double[SIZE][SIZE];

        for (int i = 0; i < SIZE; i++)
            for (int j = 0; j < SIZE; j++)
                matrix[i][j] = 0;

        for (int i = 0; i < SIZE; i++)
            matrix[i][i] = 1;
    }


    public Matrix(double angle, int type) {
        matrix = new double[SIZE][SIZE];
        double alfa = Math.toRadians(angle);
        alfa = angle;

        for (int i = 0; i < SIZE; i++)
            for (int j = 0; j < SIZE; j++)
                matrix[i][j] = 0;

        matrix[3][3] = 1;

        switch (type) {
            case ROTATE_X:
                matrix[1][1] = Math.cos(alfa);
                matrix[1][2] = Math.sin(alfa);
                matrix[2][1] = -Math.sin(alfa);
                matrix[2][2] = Math.cos(alfa);
                matrix[0][0] = 1;
                break;
            case ROTATE_Y:
                matrix[0][0] = Math.cos(alfa);
                matrix[0][2] = -Math.sin(alfa);
                matrix[2][0] = Math.sin(alfa);
                matrix[2][2] = Math.cos(alfa);
                matrix[1][1] = 1;
                break;
            case ROTATE_Z:
                matrix[0][0] = Math.cos(alfa);
                matrix[0][1] = Math.sin(alfa);
                matrix[1][0] = -Math.sin(alfa);
                matrix[1][1] = Math.cos(alfa);
                matrix[2][2] = 1;
                break;
            default:
                for (int i = 0; i < SIZE - 1; i++)
                    matrix[i][i] = 1;
                break;
        }
    }


    public Matrix(double a, double b, double c, int type) {
        matrix = new double[SIZE][SIZE];

        for (int i = 0; i < SIZE; i++)
            for (int j = 0; j < SIZE; j++)
                matrix[i][j] = 0;

        matrix[3][3] = 1;

        switch (type) {
            case SCALE:
                matrix[0][0] = a;
                matrix[1][1] = b;
                matrix[2][2] = c;
                break;
            case TRANSLATION:
                matrix[3][0] = a;
                matrix[3][1] = b;
                matrix[3][2] = c;
                // no break here
            default:
                matrix[0][0] = 1.0;
                matrix[1][1] = 1.0;
                matrix[2][2] = 1.0;
        }
    }



    public Matrix(double[][] matrix) {
        this.matrix = new double[SIZE][SIZE];
        for (int i = 0; i < SIZE; i++)
            System.arraycopy(matrix[i], 0, this.matrix[i], 0, SIZE);
    }


    public double[][] getMatrix() {
        return matrix;
    }


    public Matrix multiply(Matrix matrix2) {
        double[][] result = new double[SIZE][SIZE];
        double[][] toMultiply = matrix2.getMatrix();

        for (int i = 0; i < SIZE; i++)
            for (int j = 0; j < SIZE; j++)
                result[i][j] = newCell(matrix, toMultiply, i, j);

        return new Matrix(result);
    }


    private double newCell(double[][] matrix1, double[][] matrix2, int row, int col) {
        double cell = 0;
        for (int i = 0; i < SIZE; i++) {
            cell += matrix1[row][i] * matrix2[i][col];
        }
        return cell;
    }


    public int[] transform(Vertex p) {
        int[] xyzw = p.getXYZW();
        int[] new_xyzw = new int[4];
        int actual;

        for (int i = 0; i < 4; i++) {
            actual = 0;
            for (int j = 0; j < 4; j++) {
                actual += (xyzw[j] * matrix[j][i]);
            }
            new_xyzw[i] = actual;
        }

        //p.setXYZW(new_xyzw);
        return new_xyzw;
    }

    public double[] transformD(Vertex p) {
        int[] xyzw = p.getXYZW();
        double[] new_xyzw = new double[4];
        double actual;

        for (int i = 0; i < 4; i++) {
            actual = 0.0;
            for (int j = 0; j < 4; j++) {
                actual += ((double)xyzw[j] * matrix[j][i]);
            }
            new_xyzw[i] = actual;
        }

        //p.setXYZW(new_xyzw);
        return new_xyzw;
    }

    public double[] transformD(double[] point) {
        double[] xyzw = point;
        double[] new_xyzw = new double[4];
        double actual;

        for (int i = 0; i < 4; i++) {
            actual = 0.0;
            for (int j = 0; j < 4; j++) {
                actual += ((double)xyzw[j] * matrix[j][i]);
            }
            new_xyzw[i] = actual;
        }

        //p.setXYZW(new_xyzw);
        return new_xyzw;
    }

    public int[] transformAndNormalize(Vertex p) {
        int[] xyzw = p.getXYZW();
        int[] new_xyzw = new int[4];
        int actual;

        for (int i = 0; i < 4; i++) {
            actual = 0;
            for (int j = 0; j < 4; j++) {
                actual += (xyzw[j] * matrix[j][i]);
            }
            new_xyzw[i] = actual;
        }
        if(new_xyzw[3] != 0){
            new_xyzw[0] /= new_xyzw[3];
            new_xyzw[1] /= new_xyzw[3];
            new_xyzw[2] /= new_xyzw[3];
            new_xyzw[3] = 1;
        }

        //p.setXYZW(new_xyzw);
        return new_xyzw;
    }

    public double[] transformAndNormalizeD(Vertex p) {
        int[] xyzw = p.getXYZW();
        double[] new_xyzw = new double[4];
        double actual;

        for (int i = 0; i < 4; i++) {
            actual = 0.0;
            for (int j = 0; j < 4; j++) {
                actual += ((double)xyzw[j] * matrix[j][i]);
            }
            new_xyzw[i] = actual;
        }

        if(new_xyzw[3] != 0){
            new_xyzw[0] /= new_xyzw[3];
            new_xyzw[1] /= new_xyzw[3];
            new_xyzw[2] /= new_xyzw[3];
            //new_xyzw[3] = 1;
        }

        //p.setXYZW(new_xyzw);
        return new_xyzw;
    }

    public double[] transformAndNormalizeD(double[] point) {
        double[] xyzw = point;
        double[] new_xyzw = new double[4];
        double actual;

        for (int i = 0; i < 4; i++) {
            actual = 0.0;
            for (int j = 0; j < 4; j++) {
                actual += ((double)xyzw[j] * matrix[j][i]);
            }
            new_xyzw[i] = actual;
        }

        if(new_xyzw[3] != 0){
            new_xyzw[0] /= new_xyzw[3];
            new_xyzw[1] /= new_xyzw[3];
            new_xyzw[2] /= new_xyzw[3];
            //new_xyzw[3] = 1;
        }

        //p.setXYZW(new_xyzw);
        return new_xyzw;
    }

    // ----------------------------------------------------------------------------------------------------------------

    public static Matrix getParallelProjectionMatrix(int type) {
        switch (type) {
            case XOY:
                if (parallel_XOY == null)
                    parallel_XOY = new Matrix(new double[][]{{1, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 1}});
                return parallel_XOY;
            case XOZ:
                if (parallel_XOZ == null)
                    parallel_XOZ = new Matrix(new double[][]{{1, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 1}});
                return parallel_XOZ;
            case YOZ:
                if (parallel_YOZ == null)
                    parallel_YOZ = new Matrix(new double[][]{{0, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 1}});
                return parallel_YOZ;
            default:
                return new Matrix(new double[][]{{1, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 1}});
        }
    }

    public static Matrix getPerspectiveMatrix(int d) {
        return new Matrix(new double[][]{{1, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 0, 0}, {0, 0, 1.0 / d, 1}});
    }

    public static Matrix getPerspectiveMatrix2(double height, double width, double near, double far, double fov) {
        double a = height / width;
        //fov = Math.toRadians(fov);
        double f = 1 / Math.tan(Math.toRadians(fov/2));

        return new Matrix(new double[][]{
                {a * f, 0, 0, 0},
                {0, f, 0, 0},
                {0, 0, far / (far - near), 1},
                {0, 0, (-far * near) / (far - near), 0}
        });
    }

    public Matrix getInversed(){
        double[][] a = matrix;

        double det = a[0][0]*a[1][1]*a[2][2]*a[3][3] + a[0][0]*a[1][2]*a[2][3]*a[3][1] + a[0][0]*a[1][3]*a[2][1]*a[3][2] +
                a[0][1]*a[1][0]*a[2][3]*a[3][2] + a[0][1]*a[1][2]*a[2][0]*a[3][3] + a[0][1]*a[1][3]*a[2][2]*a[3][0] +
                a[0][2]*a[1][0]*a[2][1]*a[3][3] + a[0][2]*a[1][1]*a[2][3]*a[3][0] + a[0][2]*a[1][2]*a[2][0]*a[3][1] +
                a[0][3]*a[1][0]*a[2][2]*a[3][1] + a[0][3]*a[1][1]*a[2][1]*a[3][2] + a[0][3]*a[1][2]*a[2][1]*a[3][0] +
                -a[0][0]*a[1][1]*a[2][3]*a[3][2] - a[0][0]*a[1][2]*a[2][1]*a[3][3] - a[0][0]*a[1][3]*a[2][2]*a[3][1] +
                -a[0][1]*a[1][0]*a[2][2]*a[3][3] - a[0][1]*a[1][2]*a[2][3]*a[3][0] - a[0][1]*a[1][3]*a[2][0]*a[3][2] +
                -a[0][2]*a[1][0]*a[2][3]*a[3][1] - a[0][2]*a[1][1]*a[2][0]*a[3][3] - a[0][2]*a[1][2]*a[2][1]*a[3][0] +
                -a[0][3]*a[1][0]*a[2][1]*a[3][2] - a[0][3]*a[1][1]*a[2][2]*a[3][0] - a[0][3]*a[1][2]*a[2][0]*a[3][1];

        if(det == 0) return null;

        double[] b = new double[16];

        b[0]=a[1][1]*a[2][2]*a[3][3]+a[1][2]*a[2][3]*a[3][1]+a[1][3]*a[2][1]*a[3][2]-a[1][1]*a[2][3]*a[3][2]-a[1][2]*a[2][1]*a[3][3]-a[1][3]*a[2][2]*a[3][1];
        b[1]=a[0][1]*a[2][3]*a[3][2]+a[0][2]*a[2][1]*a[3][3]+a[0][3]*a[2][2]*a[3][1]-a[0][1]*a[2][2]*a[3][3]-a[0][2]*a[2][3]*a[3][1]-a[0][3]*a[2][1]*a[3][2];
        b[2]=a[0][1]*a[1][2]*a[3][3]+a[0][2]*a[1][3]*a[3][1]+a[0][3]*a[1][1]*a[3][2]-a[0][1]*a[1][3]*a[3][2]-a[0][2]*a[1][1]*a[3][3]-a[0][3]*a[1][2]*a[3][1];
        b[3]=a[0][1]*a[1][3]*a[2][2]+a[0][2]*a[1][1]*a[2][3]+a[0][3]*a[1][2]*a[2][1]-a[0][1]*a[1][2]*a[2][3]-a[0][2]*a[1][3]*a[2][1]-a[0][3]*a[1][1]*a[2][2];
        b[4]=a[1][0]*a[2][3]*a[3][2]+a[1][2]*a[2][0]*a[3][3]+a[1][3]*a[2][2]*a[3][0]-a[1][0]*a[2][2]*a[3][3]-a[1][2]*a[2][3]*a[3][0]-a[1][3]*a[2][0]*a[3][2];
        b[5]=a[0][0]*a[2][2]*a[3][3]+a[0][2]*a[2][3]*a[3][0]+a[0][3]*a[2][0]*a[3][2]-a[0][0]*a[2][3]*a[3][2]-a[0][2]*a[2][0]*a[3][3]-a[0][3]*a[2][2]*a[3][0];
        b[6]=a[0][0]*a[1][3]*a[3][2]+a[0][2]*a[1][0]*a[3][3]+a[0][3]*a[1][2]*a[3][0]-a[0][0]*a[1][2]*a[3][3]-a[0][2]*a[1][3]*a[3][0]-a[0][3]*a[1][0]*a[3][2];
        b[7]=a[0][0]*a[1][2]*a[2][3]+a[0][2]*a[1][3]*a[2][0]+a[0][3]*a[1][0]*a[2][2]-a[0][0]*a[1][3]*a[2][2]-a[0][2]*a[1][0]*a[2][3]-a[0][3]*a[1][2]*a[2][0];
        b[8]=a[1][0]*a[2][1]*a[3][3]+a[1][1]*a[2][3]*a[3][0]+a[1][3]*a[2][0]*a[3][1]-a[1][0]*a[2][3]*a[3][1]-a[1][1]*a[2][0]*a[3][3]-a[1][3]*a[2][1]*a[3][0];
        b[9]=a[0][0]*a[2][3]*a[3][1]+a[0][1]*a[2][0]*a[3][3]+a[0][3]*a[2][1]*a[3][0]-a[0][0]*a[2][1]*a[3][3]-a[0][1]*a[2][3]*a[3][0]-a[0][3]*a[2][0]*a[3][1];
        b[10]=a[0][0]*a[1][1]*a[3][3]+a[0][1]*a[1][3]*a[3][0]+a[0][3]*a[1][0]*a[3][1]-a[0][0]*a[1][3]*a[3][1]-a[0][1]*a[1][0]*a[3][3]-a[0][3]*a[1][1]*a[3][0];
        b[11]=a[0][0]*a[1][3]*a[2][1]+a[0][1]*a[1][0]*a[2][3]+a[0][3]*a[1][1]*a[2][0]-a[0][0]*a[1][1]*a[2][3]-a[0][1]*a[1][3]*a[2][0]-a[0][3]*a[1][0]*a[2][1];
        b[12]=a[1][0]*a[2][2]*a[3][1]+a[1][1]*a[2][0]*a[3][2]+a[1][2]*a[2][1]*a[3][0]-a[1][0]*a[2][1]*a[3][2]-a[1][1]*a[2][2]*a[3][0]-a[1][2]*a[2][0]*a[3][1];
        b[13]=a[0][0]*a[2][1]*a[3][2]+a[0][1]*a[2][2]*a[3][0]+a[0][2]*a[2][0]*a[3][1]-a[0][0]*a[2][2]*a[3][1]-a[0][1]*a[2][0]*a[3][2]-a[0][2]*a[2][1]*a[3][0];
        b[14]=a[0][0]*a[1][2]*a[3][1]+a[0][1]*a[1][0]*a[3][2]+a[0][2]*a[1][1]*a[3][0]-a[0][0]*a[1][1]*a[3][2]-a[0][1]*a[1][2]*a[3][0]-a[0][2]*a[1][0]*a[3][1];
        b[15]=a[0][0]*a[1][1]*a[2][2]+a[0][1]*a[1][2]*a[2][0]+a[0][2]*a[1][0]*a[2][1]-a[0][0]*a[1][2]*a[2][1]-a[0][1]*a[1][0]*a[2][2]-a[0][2]*a[1][1]*a[2][0];

        double[][] inversed = new double[4][4];
        int counter = 0;
        for(int i = 0;i<4;i++){
            for(int j=0;j<4;j++){
                inversed[i][j] = (1.0/det)*b[counter];
            }
            counter++;
        }

        return new Matrix(inversed);
    }

    // ----------------------------------------------------------------------------------------------------------------


    @Override
    public String toString() {
        String str = "";
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++)
                str += ("" + matrix[i][j] + " ");

            str += "\n";
        }

        return str;
    }
}
