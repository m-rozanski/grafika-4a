package Model;


import java.awt.*;

public class Light {
    Vertex v;
    Color c;

    public Light(Vertex V, Color C){
        v = V;
        c = C;
    }

    @Override
    public String toString(){
        return "Light: " + v + " " + c.toString();
    }
}
