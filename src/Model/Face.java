package Model;

import java.awt.*;
import java.util.Arrays;

public class Face {
    public Vertex[] vertices;
    public Material material;

    public Face(){
        vertices = null;
        material = null;
    }

    public Face(Vertex[] vertices, Material material) {
        this.vertices = vertices;
        this.material = material;
    }

    public void setVertices(Vertex[] vertices) {
        this.vertices = vertices;
    }

    public void setMaterial(Material material){
        this.material = material;
    }

    @Override
    public String toString(){
        return "Face: " + Arrays.deepToString(vertices) + " " + material + "\n";
    }
}
