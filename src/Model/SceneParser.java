package Model;

import java.awt.*;
import java.io.*;
import java.util.Arrays;


public class SceneParser {
    /**
     * Read scene
     *
     * @param file plik
     * @return [0] - ArrayList<Vertex>
     */
    public static Scene read(File file) {
        Vertex[] vertices = null;
        Face[] faces = null;
        Light light = null;
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));

            vertices = readVertices(reader);
            if (vertices == null) return null;

            faces = readFaces(reader, vertices);
            if (faces == null) return null;

            light = readLight(reader);
            if(light == null) return null;

            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new Scene(vertices, faces, light);
    }

    private static Face[] readFaces(BufferedReader reader, Vertex[] vertices) throws IOException {
        String line;

        // wczytywanie wierzchołków
        line = reader.readLine();
        Integer num = tryParseInt(line);
        if (num == null) return null;

        String[] xyz;
        int[] indexes = new int[3];
        Face[] faces = new Face[num];
        Vertex[] verticesOfFace = new Vertex[3];
        Integer val;
        for (int i = 0; i < num; i++) {
            line = reader.readLine();
            if (line == null) return null;

            xyz = line.split(" ");
            if (xyz.length != 3) return null;

            for (int j = 0; j < 3; j++) {
                val = tryParseInt(xyz[j]);
                if (val == null)
                    return null;

                indexes[j] = val;
            }

            for (int j = 0; j < 3; j++) {
                verticesOfFace[j] = vertices[indexes[j]];
            }

            faces[i] = new Face();
            faces[i].setVertices(new Vertex[] {vertices[indexes[0]], vertices[indexes[1]], vertices[indexes[2]]});
        }



        // załaduj indeksy materiałów
        int[] materialIndexes = new int[num];
        for (int i = 0; i < num; i++) {
            line = reader.readLine();
            if (line == null) return null;

            val = tryParseInt(line);
            if (val == null) return null;

            materialIndexes[i] = val;
        }

        Material[] materials = readMaterials(reader);
        if (materials == null) return null;

        // przyporzadkuj material każdemu trójkątowi
        for (int i = 0; i < faces.length; i++) {
            faces[i].setMaterial(materials[materialIndexes[i]]);
        }

        return faces;
    }

    private static Light readLight(BufferedReader reader) throws IOException {
        String line;

        // wczytywanie wierzchołków
        line = reader.readLine();
        if (line == null) return null;

        String[] params = line.split(" ");
        Integer val;
        if (params.length != 6) return null;

        int[] params_int = new int[6];
        for (int i = 0; i < 6; i++) {
            val = tryParseInt(params[i]);
            if (val == null) return null;
            params_int[i] = val;
        }
        Vertex v = new Vertex(params_int[0], params_int[1], params_int[2]);
        Color c = new Color(params_int[3], params_int[4], params_int[5]);

        return new Light(v, c);
    }

    private static Material[] readMaterials(BufferedReader reader) throws IOException {
        String line;

        // wczytywanie parametrów
        line = reader.readLine();
        Integer num = tryParseInt(line);
        if (num == null) return null;

        String[] params;
        int[] params_int = new int[6];
        Material[] materials = new Material[num];
        Integer val;
        for (int i = 0; i < num; i++) {
            line = reader.readLine();
            if (line == null) return null;

            params = line.split(" ");
            if (params.length != 6) return null;

            for (int j = 0; j < 6; j++) {
                val = tryParseInt(params[j]);
                if (val == null) return null;

                params_int[j] = val;
            }

            materials[i] = new Material(params_int);
        }

        return materials;
    }

    private static Vertex[] readVertices(BufferedReader reader) throws IOException {
        String line;

        // wczytywanie wierzchołków
        line = reader.readLine();
        Integer num = tryParseInt(line);
        if (num == null) return null;

        String[] xyz;
        int[] xyz_p = new int[3];
        Vertex[] vertices = new Vertex[num];
        Integer val;
        for (int i = 0; i < num; i++) {
            line = reader.readLine();
            if (line == null) return null;

            xyz = line.split(" ");
            if (xyz.length != 3) return null;

            for (int j = 0; j < 3; j++) {
                val = tryParseInt(xyz[j]);
                if (val == null)
                    return null;

                xyz_p[j] = val;
            }

            vertices[i] = new Vertex(xyz_p);
        }

        return vertices;
    }


    // ----------------------------------------------------------------------------------------------------------------
    private static Integer tryParseInt(String val) {
        try {
            return Integer.parseInt(val);
        } catch (NumberFormatException e) {
            return null;
        }
    }
}
