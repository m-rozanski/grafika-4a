import Model.*;
import UI.MainFrame;

import java.io.File;
import java.util.Arrays;


public class App {
    public static void main(String[] args) {
        //test1();
        //test2();
        //test3();
        //test4();
        //test5();
        //test6();
        //test7();
        test8();
        //test9();
    }


    private static void test9() {
        Vertex vertex = new Vertex(70, 30, 40);
        // ROTATE Y
        double alpha = -Math.atan2(vertex.x, vertex.z);
        Matrix M = new Matrix(alpha, Matrix.ROTATE_Y);
        int[] ss = M.transform(vertex);
        Vertex firstRotate = new Vertex(ss);
        print(firstRotate);
        // ROTATE X
        alpha = Math.PI / 2 - Math.atan2(firstRotate.z, firstRotate.y);
        M = new Matrix(alpha, Matrix.ROTATE_X);
        int[] sx = M.transform(firstRotate);
        Vertex secondRotate = new Vertex(sx);
        print(secondRotate);

        // first create transformatoin matrix and then transform
        Vertex vertex1 = new Vertex(70, 30, 40);

    }

    private static void test8() {
        new MainFrame();
    }

    private static void test7() {
        String path = "C:\\Users\\Mati\\Desktop\\data\\SceneParserTester.brp";
        File file = new File(path);
        Scene scene = SceneParser.read(file);
        if (scene != null) {
            scene.generateProjections();
        } else {
            print("SceneParser null");
        }
    }

    private static void test6() {
        print(Matrix.getParallelProjectionMatrix(Matrix.XOY));
        print(Matrix.getParallelProjectionMatrix(Matrix.XOZ));
        print(Matrix.getParallelProjectionMatrix(Matrix.YOZ));
        print(Matrix.getParallelProjectionMatrix(9));
    }


    private static void test5() {
        String path = "C:\\Users\\Mati\\Desktop\\data\\SceneParserTester.brp";
        File file = new File(path);
        Scene scene = SceneParser.read(file);
        if (scene != null) {
            print(scene);
        } else {
            print("SceneParser null");
        }

        path = "C:\\Users\\Mati\\Desktop\\data\\CameraParserTester.cam";
        file = new File(path);
        Camera camera = CameraParser.read(file);
        if (camera != null) {
            print(camera);
        } else {
            print("CameraParser null");
        }

        path = "C:\\Users\\Mati\\Desktop\\data\\CameraParserTesterOutput.cam";
        if (camera != null)
            CameraParser.save(camera, path);
    }


    private static void test4() {
        String s = "4";
        Integer i = tryParseInt(s);
        print(i);
    }

    private static void test3() {
        Vertex p = new Vertex(1, 2, 3, 2);
        print(p);
        p.normalize();
        print(p);
    }

    private static void test2() {
        double[][] m = new double[][]{{1, 2, 3, 4}, {1, 2, 3, 4}, {1, 2, 3, 4}, {1, 2, 3, 4}};
        Matrix M = new Matrix(m);
        Vertex p = new Vertex(1, 2, 3, 1);

        M.transform(p);
        print(p);
    }

    private static void test1() {
        Matrix m = new Matrix();
        System.out.println("Empty: \n" + m);
        m = new Matrix(45, Matrix.ROTATE_X);
        System.out.println("\nX: \n" + m);
        m = new Matrix(45, Matrix.ROTATE_Y);
        System.out.println("\nY: \n" + m);
        m = new Matrix(45, Matrix.ROTATE_Z);
        System.out.println("\nZ: \n" + m);
        m = new Matrix(3, 4, 5, Matrix.SCALE);
        System.out.println("\nScale: \n" + m);
        m = new Matrix(3, 4, 5, Matrix.TRANSLATION);
        System.out.println("\nTranslation: \n" + m);
        double[][] m1 = new double[][]{{2, 5, 6, 3}, {6, 5, 8, 1}, {4, 9, 1, 3}, {5, 2, 8, 2}};
        double[][] m2 = new double[][]{{4, 3, 5, 2}, {5, 9, 7, 1}, {1, 2, 3, 4}, {4, 6, 3, 8}};
        m = new Matrix(m1);
        System.out.println("\nMul: \n" + m.multiply(new Matrix(m2)));
    }

    // ----------------------------------------------------------------------------------------------------------------
    private static void print(Object... values) {
        for (Object c : values) {
            if (c.getClass().isArray())
                System.out.println(Arrays.deepToString((Object[]) c));
            else
                System.out.println(c);
        }
    }

    private static Integer tryParseInt(String val) {
        try {
            return Integer.parseInt(val);
        } catch (NumberFormatException e) {
            return null;
        }
    }
}


